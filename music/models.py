from django.db import models

# Create your models here.
class Song(models.Model):
    name = models.TextField()
    song = models.FileField(upload_to="music/")