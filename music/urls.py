from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<int:id>/", views.get_song, name="get_song_by_id"),
    path("add_song", views.add_song, name="add_song")
]