from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect

from .models import Song

def index(request):
    return render(
        request,
        "music/index.html", 
        {"songs": Song.objects.all()}
    )
    
def get_song(request, id):
    return render(request, "music/song.html", {"song": get_object_or_404(Song, pk=id)})
    
def add_song(request):
    if request.method == "POST":
        s = Song(name=request.POST["name"], song=request.FILES["song"])
        s.save()
        return HttpResponseRedirect(f"#{s.pk}")
    else:
        return render(request, "music/add_song.html")